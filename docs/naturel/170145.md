---
title: Énigme 2
author: N. Revéret
hide:
  - footer
---

# Énigme `2`

Félicitations, vous avez saisi le concept de variable. Compliquons un peu les choses !

??? question "Lire ⇨ Calculer ⇨ Affecter ⇨ Recommencer !"

    On considère l'algorithme ci-dessous :
    
    ```texte
    a = 1
    a = a × 2
    a = a × 3
    a = a × 5
    a = a × 7
    ```
    
    Combien vaut `a` désormais ?
    
    Ici, on ne fournit pas de terminal car il est possible de faire le calcul de tête !


??? tip "Aide"

    Il faut lire chaque ligne à l'envers :
    
    * Pour la première ligne :
        * je prends la valeur de `a` ;
        * je la multiplie par `#!py 2` ;
        * j'affecte cette valeur à la variable `a` ;
    * Pour la deuxième ligne :
        * je prends la nouvelle valeur de `a` ;
        * je la multiplie par `#!py 3` ;
        * j'affecte cette valeur à la variable `a` ;
    * etc.





