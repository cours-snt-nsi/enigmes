---
title: Énigme 8
author: N. Revéret
hide:
  - footer
---

# Énigme `8`

Bien joué ! Les boucles « Pour » permettent donc de répéter des actions un certain nombre de fois.

Le nombre de répétitions n'est pas toujours simple à déterminer...

??? question "Dépasser les bornes"

    On considère l'algorithme ci-dessous :

    ```texte
    a = 0
    Pour chaque nombre entre 83 et 157 (inclus tous les deux) :
        a = a + 5
    ```

    Combien vaut `a` à l'issue du code ?
    
    {{ terminal() }}


??? tip "Aide"

    L'intervalle $\left[83\,;\,84\right]$ contient deux nombres entiers.
    
    L'intervalle $\left[83\,;\,85\right]$ contient trois nombres entiers.
    
    ...
    
    L'intervalle $\left[83\,;\,100\right]$ contient dix-huit nombres entiers.





