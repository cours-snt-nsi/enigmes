---
title: Énigme 15
author: N. Revéret
hide:
  - footer
---

# Énigme `15`

Bravo, vous venez de convertir `#!py 427` en binaire : `#!py 110101011`.

Terminons avec une dernière énigme un peu plus compliquée pour les téméraires !

??? question "*Fast exponentiation*"

    Combien vaut `résultat` à l'issue de cet algorithme ?
    
    ```texte
    x = 12
    e = 9
    résultat = 1
    Tant que e est non nul :
        Si e est impair :
            résultat = résultat * x
        x = x * x
        e = e // 2
    ```
    
    {{ terminal() }}
    
??? tip "Aide"

    La division par `#!py 2` est un quotient entier : `#!py 9 // 2` vaut `#!py 4` !