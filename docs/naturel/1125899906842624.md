---
title: Énigme 11
author: N. Revéret
hide:
  - footer
---

# Énigme `11`

Eh bien... Tout cela en seulement deux boucles !

Jusqu'à maintenant, les boucles « Pour » utilisées parcouraient des ensembles de nombres. De façon générale, il est possible de parcourir des ensembles contenant des éléments de types variés.

Par exemple des caractères ⬇


??? question "Encore des chiffres et des lettres"

    Combien vaut `total` à l'issue de cet algorithme ?

    ```texte
    total = 0
    phrase = "Ceci est une phrase constituée de 49 caractères !"
    Pour chaque caractère de phrase :
        Si caractère est une voyelle :
            Ajouter 139 à total
       Sinon :
            Soustraire 39 à total
    ```

    {{ terminal() }}

??? tip "Aide"

    La boucle parcourt les **caractères** de `phrase` : les lettres, les chiffres **et** la ponctuation !





