---
author: N. Revéret
title : Énigmes algorithmiques
---

# Énigmes algorithmiques

Bienvenue !

Vous êtes sur un site de découverte de l'algorithmique à l'aide d'énigmes.

Sur chaque page, une énigme vous est proposée. Elles sont de difficultés croissantes.

Une fois l'énigme résolue, pour accéder à la page suivante, vous devez simplement remplacer la dernière partie de l'adresse du site par votre solution.

Prêt à essayer ? Nous vous proposons plusieurs parcours, de difficultés croissantes mais surtout adaptés à votre aisance en algorithmique. Vous pouvez débuter par le parcours de votre choix.

!!! example "Les différents parcours"

    🐭 [« **Bases de calcul** »](calculs/1.md) : découvrir les opérations et leur écriture sur l'ordinateur ;

    😺 [« **Le langage naturel** »](naturel/1.md) : découvrir les structures fondamentales de la programmation en français.

<!-- 🐺 [« **Bases de Python** »](bases/1.md) : découvrir les bases de `Python` ;

🐻 [« **Problèmes en Python** »](problemes/1.md) : des problèmes en `Python`. -->